package uz.elmurodov.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import uz.elmurodov.mybeans.Person;

/**
 * @author Elmurodov Javohir, Wed 2:05 PM. 12/8/2021
 */


@Configuration
public class SpringConfig {
    @Bean
    public Person person() {
        return new Person();
    }
}
