package uz.elmurodov;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import uz.elmurodov.configs.SpringConfig;
import uz.elmurodov.mybeans.Person;

/**
 * @author Elmurodov Javohir, Wed 2:05 PM. 12/8/2021
 */
public class App {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(SpringConfig.class);
        Person person = context.getBean(Person.class);
        System.out.println(person);
    }
}
