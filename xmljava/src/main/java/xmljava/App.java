package xmljava;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import xmljava.mybeans.Person;

/**
 * @author Elmurodov Javohir, Wed 1:33 PM. 12/8/2021
 */
public class App {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml");
//        Person person = context.getBean("person", Person.class);
        Person person = context.getBean(Person.class);
        System.out.println(person);
        context.close();

    }
}
