package xmljava.mybeans;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @author Elmurodov Javohir, Wed 1:37 PM. 12/8/2021
 */

@Getter
@Setter
@Component
@ToString
@PropertySource(value = "classpath:my.properties")
@PropertySource(value = "classpath:age.properties")
public class Person {

    @Value("${fullName:7777}")
    private String fullName;

    @Value("${age}")
    private int age;

    @PostConstruct
    private void init() {
        System.out.println("I'm a method that works after initializing the bean");
    }

    @PreDestroy
    private void destroy() {
        System.out.println("I'm destroying the bean 🤣");
    }


}
