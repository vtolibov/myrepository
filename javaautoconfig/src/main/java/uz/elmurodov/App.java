package uz.elmurodov;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import uz.elmurodov.config.SpringConfig;
import uz.elmurodov.models.Employee;
import uz.elmurodov.models.Person;

/**
 * @author Elmurodov Javohir, Wed 2:08 PM. 12/8/2021
 */
public class App {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(SpringConfig.class);
        Person person = context.getBean("person", Person.class);
        Employee employee = context.getBean("employee", Employee.class);
        System.out.println(person);
        System.out.println(employee);
        context.close();
    }
}

