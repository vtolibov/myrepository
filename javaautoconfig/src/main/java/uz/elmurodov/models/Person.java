package uz.elmurodov.models;

import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @author Elmurodov Javohir, Wed 2:09 PM. 12/8/2021
 */

@ToString
@Component("person")
//@PropertySource("classpath:app.properties")
public class Person {

    @Value("${fullName:Sorry FullName is not found}")
    private String fullName;

    @Value("${age:0}")
    private int age;
}
