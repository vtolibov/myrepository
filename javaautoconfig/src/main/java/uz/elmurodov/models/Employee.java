package uz.elmurodov.models;

import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @author Elmurodov Javohir, Wed 2:11 PM. 12/8/2021
 */

@ToString
@Component
//@PropertySource("classpath:app.properties")
public class Employee {

    @Value("${position:Sorry position not found}")
    private String position;
}
