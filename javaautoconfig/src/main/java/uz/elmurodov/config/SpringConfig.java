package uz.elmurodov.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author Elmurodov Javohir, Wed 2:09 PM. 12/8/2021
 */
@Configuration
@ComponentScan("uz.elmurodov.*")
@PropertySource("classpath:app.properties")
public class SpringConfig {
}
