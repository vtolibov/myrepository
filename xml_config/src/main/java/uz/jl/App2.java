package uz.jl;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import uz.jl.models.Employee;

import java.util.ArrayList;

/**
 * @author Elmurodov Javohir, Sun 12:05 PM. 11/28/2021
 */
public class App2 {
    public static void main(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml");
        Employee employee = context.getBean("emp", Employee.class);
//        System.out.println(employee);
        employee.getSkills().forEach((k, v) -> System.out.println(k + " : " + v));
        employee.getEmployeeSkill().forEach((k, v) -> System.out.println(k + " : " + v));

        employee.setMySkillsList(new ArrayList<>());

    }
}
