package uz.jl;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import uz.jl.models.Employee;

import static uz.jl.utils.Color.GREEN;
import static uz.jl.utils.Color.RED;
import static uz.jl.utils.Print.println;

/**
 * @author Elmurodov Javohir, Sat 12:47 PM. 11/27/2021
 */
public class App {
    public static void main(String[] args) {
        Resource resource = new ClassPathResource("applicationContext.xml");
        BeanFactory factory = new XmlBeanFactory(resource);
        Employee employee1 = (Employee) factory.getBean("emp");
        Employee employee2 = (Employee) factory.getBean("emp");
        Employee employee3 = factory.getBean("emp", Employee.class);
        Employee employee4 = factory.getBean("emp", Employee.class);

        if (factory.containsBean("emp2")) {
            System.out.println("factory.containsBean(emp2)");
        }
        if (factory.containsBean("emp")) {
            System.out.println("factory.containsBean(emp)");
        }

        if (factory.isPrototype("emp")) {
            println(GREEN, "Yes");
        }

//        System.out.println(employee1.getAge());
//        System.out.println(employee2.getAge());
//        System.out.println(employee3.getAge());
//        System.out.println(employee4.getAge());
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        Employee employee = applicationContext.getBean("emp2", Employee.class);
        System.out.println(employee);
        System.out.println(applicationContext.getApplicationName());

        for (String beanDefinitionName : applicationContext.getBeanDefinitionNames()) {
            System.out.println(beanDefinitionName);
        }



    }
}
