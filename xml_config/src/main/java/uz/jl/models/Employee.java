package uz.jl.models;

import jdk.jshell.Snippet;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Elmurodov Javohir, Sat 12:52 PM. 11/27/2021
 */

@Getter
@Setter
@ToString
@AllArgsConstructor
public class Employee {
    private int age;
    private String name;
    private Passport passport;
    private Map<String, String> skills;
    private List<Skill> mySkillsList;
    private Set<Skill> mySkillsSet;
    private Map<Person, Skill> employeeSkill;


}
