package uz.jl.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Elmurodov Javohir, Sun 12:04 PM. 11/28/2021
 */

//@Setter
//@Getter
@ToString
@AllArgsConstructor
public class Passport {
    private String serial;
    private String number;
}
