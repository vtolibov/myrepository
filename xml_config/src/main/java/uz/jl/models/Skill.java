package uz.jl.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Elmurodov Javohir, Sun 12:36 PM. 11/28/2021
 */

@Setter
@Getter
@ToString
public class Skill {
    private String name;
}
