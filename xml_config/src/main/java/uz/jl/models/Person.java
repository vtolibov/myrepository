package uz.jl.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Elmurodov Javohir, Sun 12:36 PM. 11/28/2021
 */
@Getter
@Setter
//@ToString
public class Person {
    private String fullName;
    private static Person person;


    public static Person createInstance() {
//        System.out.println("Factory Method is working");
//        if (person == null) {
//            person = new Person();
//        }
        return new Person();
    }

    public void init() {
        System.out.println("Bean qurulayapti");
    }

    public void destroy() {
        System.out.println("Bean destroy qilinyapti");
    }

    // init method -> before initializing bean
    // destroy method -> after destroy bean
    // factory method -> defines how to create the instance of the bean  | AbstractFactory (cretional design pattern)

}
