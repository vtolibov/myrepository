package uz.jl;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import uz.jl.models.Person;

/**
 * @author Elmurodov Javohir, Wed 1:20 PM. 12/8/2021
 */
public class App3 {
    public static void main(String[] args) {
//        1-version
//        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext();
//        context.setConfigLocation("applicationContext.xml");
//        context.refresh();
        // 2-version
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Person person = context.getBean("person2", Person.class);
        Person person3 = context.getBean("person2", Person.class);
//        System.out.println(person.getFullName());
        System.out.println(person);
        System.out.println(person3);
        context.close();
        context.close();
    }
}
